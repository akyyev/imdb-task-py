from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class Top250Page(BasePage):

    url = 'https://www.imdb.com/search/title/?groups=top_250&sort=user_rating'

    @property
    def top250_film_list_element(self):
        locator = (By.XPATH, "//div[@class='lister-list']/div")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def next_page_element(self):
        locator = (By.CSS_SELECTOR, ".nav > .desc > .lister-page-next.next-page")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
