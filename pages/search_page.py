from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class SearchPage(BasePage):

    url = None

    @property
    def movie_filter(self):
        locator = (By.LINK_TEXT, "Movie")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def result_list(self):
        locator = (By.XPATH, "//table[@class='findList']//tbody//tr//td[2]//a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
