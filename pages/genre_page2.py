from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class GenrePage2(BasePage):

    url = None

    @property
    def user_rating_filter(self):
        locator = (By.LINK_TEXT, "User Rating")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def user_rating_filter2(self):
        locator = (By.CSS_SELECTOR, "select[name='sort']")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def compact_view(self):
        locator = (By.CSS_SELECTOR, ".compact")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def compact_view2(self):
        locator = (By.CSS_SELECTOR, "span[title='Compact view']")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def movie_list(self):
        locator = (By.XPATH, "//div[@class='lister-list']//div[@class='col-title']//span//span[2]//a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
