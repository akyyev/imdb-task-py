# 🎁 Selenium Task Python

Python pytest selenium ile IMDB projesi...


### Ön Koşullar

config.ini dosyasında path alanına ilgili chromedriver versiyonu yolu belirtilmelidir.

config.ini dosyasına arama yapılacak film adı girilmelidir.

config.ini dosyasında write_json_file parametresi on yapılırsa sonuçlar json formatında dosyaya yazılır.

Aşağıdaki komut ile bağımlılıklar yüklenmelidir.
~~~
pipenv install
~~~

Aşağıdaki komut ile sanal ortam aktif edilmelidir.
~~~
pipenv shell
~~~


### Test

IMDB'deki top 250 filmi listelemek için...
~~~
pipenv run python -m pytest -s -m Top250
~~~

IMDB'de film aramak için...
~~~
pipenv run python -m pytest -s -m Search
~~~

IMDB'de bütün kategorileri listelemek için...
~~~
pipenv run python -m pytest -s -m Categories
~~~
