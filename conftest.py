# -*- coding: utf-8 -*-

from pytest import fixture
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

from utils.helper import read_config


@fixture(scope='session')
def browser():
    options = Options()
    # options.add_argument('--disable-extensions')
    # options.add_argument('--disable-logging')
    # options.binary_location = 'C:/bin/chromedriver.exe'
    options.add_argument('--log-level=3')

    # CHROME DRIVER SLIENT MODDA AÇILIYOR
    driver_config = read_config('res/config.ini', 'driver')
    if driver_config['silent_mode'] == 'on':
        options.add_argument('headless')

    driver = Chrome(executable_path=driver_config['path'], options=options)
    # driver = Chrome()
    driver.implicitly_wait(10)
    driver.maximize_window()
    yield driver
    driver.quit()


@fixture(scope='session')
def config():
    result = read_config('res/config.ini', 'base')
    return result
