# -*- coding: utf-8 -*-

from pytest import mark
from selenium.common.exceptions import TimeoutException

from pages.genre_page import GenrePage
from pages.genre_page2 import GenrePage2
from pages.home_page import HomePage
from pages.movie_page import MoviePage
from pages.search_page import SearchPage
from pages.top250_page import Top250Page
from utils.helper import long_string_handle, write_json_to_file, title_with_year_handle


class TestImdb:

    @mark.Top250
    def check_top_250_movie(self, browser, config):

        write_json_file = config['write_json_file']

        top250_page = Top250Page(browser)
        top250_page.load()

        print()
        print('%-10s %-40s %-10s %-10s %s' % ('order', 'title', 'year', 'rating', 'genres'))

        json_movie_info_list = []

        while True:
            try:
                top250_film_list = top250_page.top250_film_list_element.finds_to()

                for movie in top250_film_list:

                    order_element = movie.find_element_by_css_selector(".lister-item-index.text-primary.unbold")
                    order = order_element.text

                    title_element = movie.find_element_by_css_selector(".lister-item-content > h3 >a")
                    title = title_element.text
                    title = long_string_handle(title)

                    year_element = movie.find_element_by_css_selector(".lister-item-year.text-muted.unbold")
                    year = year_element.text
                    if '(I) ' in year:
                        year = year.replace('(I) ', '')
                    year = year.replace('(', '').replace(')', '')

                    rating_element = movie.find_element_by_css_selector(".ratings-bar > div > strong")
                    rating = rating_element.text

                    genres_element = movie.find_element_by_css_selector(".lister-item-content > p .genre")
                    genres = genres_element.text.split(',')

                    # FİLM BİLGİLERİ EKRANA YAZDIRILIYOR
                    print('%-10s %-40s %-10s %-10s %s' % (order, title, year, rating, genres))

                    # FİLM BİLGİLERİ JSON DOSYASINA YAZMAK İÇİN LİSTEYE EKLENİYOR
                    json_movie_info_list.append(
                        {'order': order, 'title': title, 'year': year, 'rating': rating, 'genres': genres})

                # SONRAKİ SAYFA AÇILIYOR
                top250_page.next_page_element.to_click()

            except TimeoutException:
                break

        # SONUÇLARDAN DICT OLUŞTURULUP JSON FORMATINDA YAZILIYOR
        if write_json_file == 'on':
            json_result_dict = {'name': 'Top 250 Movie', 'result': json_movie_info_list}
            write_json_to_file('res/Top_250_Movie.json', json_result_dict)

    @mark.Search
    def check_search_movie(self, browser, config):

        write_json_file = config['write_json_file']
        search_keyword = config['search_keyword']
        search_result_count = int(config['search_result_count'])

        # ANA SAYFA AÇILIYOR
        home_page = HomePage(browser)
        home_page.load()

        # FİLM ARAMA YAPILYOR
        home_page.search_input.to_send_keys(search_keyword)
        home_page.search_button.to_click()

        # ARAMA SAYFASINDA MOVIE FİLTRESİNE TIKLANIYOR
        search_page = SearchPage(browser)
        search_page.movie_filter.to_click()

        # ARAMA SONUÇLARI
        search_result_list = search_page.result_list.finds_to()

        # SONUÇLARDAN search_result_count KADAR FİLMİ LİSTELEYECEĞİZ
        del search_result_list[search_result_count:]

        # ARAMA SONUÇLARINDAN FİLMLERİN LİNKLERİ AYRI BİR LİSTEYE ALINIYOR
        movie_href_list = []
        for movie in search_result_list:
            movie_href_list.append(movie.get_attribute('href'))

        print()
        print('%-40s %-10s %-10s %s' % ('title', 'year', 'rating', 'genres'))

        json_movie_info_list = []

        for movie_href in movie_href_list:

            # FİLM SAYFASI AÇILIYOR
            movie_page = MoviePage(browser)
            movie_page.load_url(movie_href)

            # TITLE BİLGİSİ
            title = movie_page.title.get_text_to
            title = title_with_year_handle(title)

            # YEAR BİLGİSİ
            year = movie_page.year.get_text_to

            # PUANLAMASI YAPILMAMIŞ GELECEK FİLMLER DE DÜŞÜNÜLEREK RATING BİLGİSİ
            try:
                rating = movie_page.rating.get_text_to
            except TimeoutException:
                rating = None

            # GENRE BİLGİSİ
            genre_list = movie_page.genre_list.finds_to()
            genres = []
            for genre in genre_list:
                if 'genres' in genre.get_attribute('href'):
                    genres.append(genre.text)

            # FİLM BİLGİLERİ EKRANA YAZDIRILIYOR
            print('%-40s %-10s %-10s %s' % (title, year, rating, genres))

            # FİLM BİLGİLERİ JSON DOSYASINA YAZMAK İÇİN LİSTEYE EKLENİYOR
            json_movie_info_list.append({'title': title, 'year': year, 'rating': rating, 'genres': genres})

        # SONUÇLARDAN DICT OLUŞTURULUP JSON FORMATINDA YAZILIYOR
        if write_json_file == 'on':
            json_result_dict = {'name': search_keyword, 'result': json_movie_info_list}
            write_json_to_file('res/%s.json' % search_keyword, json_result_dict)

    @mark.Categories
    def check_popular_movies_by_genre(self, browser, config):

        write_json_file = config['write_json_file']

        # Popular Movies by Genre SAYFASI AÇILIYOR
        genre_page = GenrePage(browser)
        genre_page.load()

        # FİLM TÜRLERİ LİSTELENİYOR
        genre_list = genre_page.genre_list.finds_to()

        movie_list_by_genre = []

        print()

        for idx, genre in enumerate(genre_list):
            print('{} - {}'.format(idx+1, genre.text))
            movie_list_by_genre.append((genre.text, genre.get_attribute('href')))

        # KULLANICIDAN TÜR SEÇMESİ BEKLENİYOR
        while True:
            selected_genre = input('\nLütfen yukarıdaki listeden bir tür seçiniz: ')
            try:
                int_selected_genre = int(selected_genre) - 1
                if int_selected_genre == -1:
                    print('Lütfen listelenen numaralardan birini seçiniz...')
                else:
                    genre_page2 = GenrePage2(browser)
                    genre_page2.load_url(movie_list_by_genre[int_selected_genre][1])
                    break
            except ValueError:
                print('Lütfen numara belirterek tür seçiniz...')
            except IndexError:
                print('Lütfen listelenen numaralardan birini seçiniz...')

        # Superhero TÜRÜNDE SAYFA YAPISI FARKLI
        try:
            genre_page2.user_rating_filter.to_click()
        except TimeoutException:
            genre_page2.user_rating_filter2.to_select_by_visible_text('IMDb Rating')

        # Superhero TÜRÜNDE SAYFA YAPISI FARKLI
        try:
            genre_page2.compact_view.to_click()
        except TimeoutException:
            genre_page2.compact_view2.to_click()

        movie_list = genre_page2.movie_list.finds_to()
        del movie_list[10:]

        print()
        print('Top 10 {} movie'.format(movie_list_by_genre[int_selected_genre][0]))

        json_movie_list = []
        for movie in movie_list:
            title = movie.text
            print(title)
            json_movie_list.append(title)

        # SONUÇLARDAN DICT OLUŞTURULUP JSON FORMATINDA YAZILIYOR
        if write_json_file == 'on':
            json_result_dict = {'name': movie_list_by_genre[int_selected_genre][0], 'result': json_movie_list}
            write_json_to_file('res/top_10_%s.json' % movie_list_by_genre[int_selected_genre][0], json_result_dict)
