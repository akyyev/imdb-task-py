import json
from configparser import ConfigParser


def long_string_handle(text):
    if len(text) > 40:
        text = '%s...' % text[:35]
    return text


def title_with_year_handle(text):
    text = text[:-7]
    text = long_string_handle(text)
    return text


def read_config(filename, section):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename, encoding='utf-8')

    # get section, default to postgresql
    result = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            result[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return result


def write_json_to_file(json_file, data):
    try:
        with open(json_file, 'w', encoding='utf-8') as open_file:
            json.dump(data, open_file, ensure_ascii=False, indent=2)
        print('\n{} dosyası başarıyla yazıldı...\n'.format(json_file))
        return True

    except Exception as e:
        print('\nJson dosyası yazılamadı! {}\n'.format(e))
        return False
