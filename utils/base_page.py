
class BasePage(object):

    url = None

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.url)

    def load_url(self, url):
        self.browser.get(url)

    def refresh_page(self):
        self.browser.refresh()

    def scroll_down(self):
        self.browser.execute_script("window.scrollTo(0, 500);")
